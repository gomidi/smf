package main

import (
	"fmt"
	"os"

	"gitlab.com/golang-utils/config/v2"
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/reader"
)

var CAT = cat{}

func init() {
	CAT.init()
}

type cat struct {
	*config.Config
	file config.StringGetter
}

func (c *cat) init() {
	c.Config = CONFIG.Command("cat", "cat shows the content of an SMF (MIDI) file").Skip("midifile")
	c.file = c.LastString("midifile", "the midi file that should be shown", config.Required())
}

func (c *cat) print() error {
	rd := reader.New(reader.NoLogger(),
		reader.Each(func(pos *reader.Position, msg midi.Message) {
			fmt.Fprintf(os.Stdout, "T%v [%v] %s\n", pos.Track, pos.AbsoluteTicks, msg.String())
		}),
	)

	return reader.ReadSMFFile(rd, c.file.Get())
}
