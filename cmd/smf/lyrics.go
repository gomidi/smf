package main

import (
	"fmt"
	"os"

	"gitlab.com/golang-utils/config/v2"
	"gitlab.com/gomidi/smf/lyrics"
)

var LYRICS = lyr{}

func init() {
	LYRICS.init()
}

type lyr struct {
	*config.Config
	file        config.StringGetter
	track       config.IntGetter
	includeText config.BoolGetter
	asJson      config.BoolGetter
}

func (c *lyr) init() {
	c.Config = CONFIG.Command("lyrics", "extracts lyrics from a SMF file, tracks are separated by an empty line").Skip("midifile")
	c.file = c.LastString("file", "the SMF file", config.Required())
	c.track = c.Int(
		"track",
		"the track from which the lyrics are taken. If not set means all tracks, 0 is the first, 1 the second etc",
		config.Shortflag('t'),
	)

	c.includeText = c.Bool(
		"text",
		"include free text entries in the SMF file. Text is surrounded by doublequotes",
	)

	c.asJson = c.Bool(
		"json",
		"output json format",
		config.Shortflag('j'),
	)
}

func (c *lyr) print() error {
	var options []lyrics.Option
	if c.track.IsSet() {
		options = append(options, lyrics.OptionTrackNo(uint16(c.track.Get())))
	}

	if c.includeText.Get() {
		options = append(options, lyrics.OptionIncludeText())
	}

	if c.asJson.Get() {
		options = append(options, lyrics.OptionJSONOutput())
	}

	str, err := lyrics.Read(c.file.Get(), options...)

	if err != nil {
		return err
	}

	fmt.Fprintln(os.Stdout, str)
	return nil
}
