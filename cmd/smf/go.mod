module gitlab.com/gomidi/smf/cmd/smf

go 1.24.0

require (
	gitlab.com/golang-utils/config/v2 v2.12.0
	gitlab.com/gomidi/midi v1.20.3
	gitlab.com/gomidi/smf v0.0.7
	gitlab.com/gomidi/smf/ui v0.0.7
)

require (
	github.com/atotto/clipboard v0.1.4 // indirect
	github.com/aymanbagabas/go-osc52/v2 v2.0.1 // indirect
	github.com/charmbracelet/bubbles v0.20.0 // indirect
	github.com/charmbracelet/bubbletea v1.3.3 // indirect
	github.com/charmbracelet/lipgloss v1.0.0 // indirect
	github.com/charmbracelet/x/ansi v0.8.0 // indirect
	github.com/charmbracelet/x/term v0.2.1 // indirect
	github.com/emersion/go-appdir v1.1.2 // indirect
	github.com/erikgeiser/coninput v0.0.0-20211004153227-1c3628e74d0f // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell v1.4.0 // indirect
	github.com/gdamore/tcell/v2 v2.7.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-localereader v0.0.1 // indirect
	github.com/mattn/go-runewidth v0.0.16 // indirect
	github.com/moby/sys/mountinfo v0.7.2 // indirect
	github.com/muesli/ansi v0.0.0-20230316100256-276c6243b2f6 // indirect
	github.com/muesli/cancelreader v0.2.2 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.15.2 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/rivo/tview v0.0.0-20240225120200-5605142ca62e // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/zs5460/art v0.3.0 // indirect
	gitlab.com/golang-utils/dialog v0.2.1 // indirect
	gitlab.com/golang-utils/errors v0.0.2 // indirect
	gitlab.com/golang-utils/fmtdate v1.0.2 // indirect
	gitlab.com/golang-utils/fs v0.20.1 // indirect
	gitlab.com/golang-utils/scaffold v1.15.7 // indirect
	gitlab.com/golang-utils/updatechecker v0.0.11 // indirect
	gitlab.com/golang-utils/version v1.0.4 // indirect
	golang.org/x/sync v0.11.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/term v0.29.0 // indirect
	golang.org/x/text v0.22.0 // indirect
)

replace gitlab.com/gomidi/smf => ../../

replace gitlab.com/gomidi/smf/ui => ../../ui
