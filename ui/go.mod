module gitlab.com/gomidi/smf/ui

go 1.22.1

toolchain go1.22.2

require (
	github.com/gdamore/tcell v1.4.0
	github.com/gdamore/tcell/v2 v2.7.1
	github.com/rivo/tview v0.0.0-20240225120200-5605142ca62e
	gitlab.com/gomidi/midi v1.20.3
	gitlab.com/gomidi/smf v0.0.7
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	gitlab.com/golang-utils/version v1.0.1 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/term v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)

replace gitlab.com/gomidi/smf => ../
